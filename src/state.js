import { createStore } from "vuex";
import router from "./router";

const backendUrl = process.env.VUE_APP_BACKEND_URL;
const clientId = process.env.VUE_APP_CLIENT_ID;
const clientSecret = process.env.VUE_APP_CLIENT_SECRET;

const store = createStore({
  state: {
    user: {},
    token: null,
    preLoading: false,
    dataPreLoading: true,
    errorPassword: false,
    loginError: false,
    loggedIn: false,
    apartments: {},
    // pager: {
    //     currentPage: 1,
    //     perPage: 5,
    // },
    validation: {},
    createApartmentDialogVisible: false,
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    },
    getToken(context, token) {
      context.token = token;
    },
    setUser(context, user) {
      context.user = user;
    },
    setPreloading(context, isLoad) {
      context.preLoading = isLoad;
    },
    setDataPreloading(context, isLoad) {
      context.dataPreLoading = isLoad;
    },
    setErrorPassword(context, isError) {
      context.errorPassword = isError;
    },
    setLoginError(context, isError) {
      context.loginError = isError;
    },
    setLoggedIn(context, isLoggedIn) {
      context.loggedIn = isLoggedIn;
    },
    setApartments(context, apartments) {
      context.apartments = apartments;
    },
    // setPage(context, page) {
    //     context.pager.currentPage = page;
    // },
    // setPager(context, pager) {
    //     context.pager = pager;
    // },
    // setPerPage(context, rows) {
    //     context.pager.perPage = rows;
    // },
    setValidation(context, validation) {
      context.validation = validation;
    },
    setCreateApartmentDialogVisible(context, visible) {
      context.createApartmentDialogVisible = visible;
    },
    logout(context) {
      context.user = null;
      context.token = null;
      context.loggedIn = false;
      context.apartments = {};
      localStorage.removeItem("token");
      router.push("/");
    },
  },
  actions: {
    auth(context, { login, password }) {
      context.commit(`setErrorPassword`, false);
      window.axios
        .post(
          `${backendUrl}/OAuthController/authorize`,
          {
            username: login,
            password: password,
            grant_type: `password`,
          },
          {
            headers: {
              Authorization: `Basic ${window.btoa(
                clientId + `:` + clientSecret
              )}`,
            },
          }
        )
        .then((response) => {
          if (response.data.access_token) {
            context.commit(`setToken`, response.data.access_token);
            localStorage.setItem(`token`, response.data.access_token);
            context.commit(`setLoggedIn`, true);
            context.commit(`setLoginError`, false);
            context.dispatch(`getUser`);
          } else {
            context.commit(`setLoginError`, true);
            context.commit(`setPreloading`, false);
            context.commit(`setErrorPassword`, true);
          }
          router.push(`/`);
        });
    },
    getUser(context) {
      context.commit(`setPreloading`, true);
      return window.axios
        .get(`${backendUrl}/OAuthController/user`, {
          headers: {
            Authorization: `Bearer ${context.state.token}`,
          },
        })
        .then((response) => {
          context.commit(`setUser`, response.data);
          context.commit(`setPreloading`, false);
        })
        .catch((error) => {
          if (error.response) {
            context.commit(`setLoggedIn`, false);
          }
        });
    },
    getApartments(context) {
      context.commit(`setDataPreloading`, true);
      const params = new URLSearchParams();
      // params.append('per_page', context.state.pager.perPage);
      return window.axios
        .post(
          `${backendUrl}/Api/getApartments?page_group1=${
            /* context.state.pager.currentPage */ 1
          }`,
          params,
          {
            headers: {
              Authorization: `Bearer ${context.state.token}`,
            },
          }
        )
        .then((response) => {
          context.commit(`setApartments`, response.data.apartments);
          // context.commit('setPager', response.data.pager);
          context.commit(`setDataPreloading`, false);
        });
    },
    createApartment(context, { country, city, address, description, picture }) {
      context.commit(`setDataPreloading`, true);
      const formData = new FormData();
      formData.append(`country`, country);
      formData.append(`city`, city);
      formData.append(`address`, address);
      formData.append(`description`, description);
      formData.append(`picture`, picture);

      return window.axios
        .post(`${backendUrl}/Api/store`, formData, {
          headers: {
            Authorization: `Bearer ${context.state.token}`,
          },
        })
        .then((response) => {
          if (response.status === 201) {
            context.commit(`setValidation`, {});
            context.commit(`setCreateApartmentDialogVisible`, false);
            this.dispatch(`getApartments`);
            router.push(`/apartments`);
          } else {
            context.commit(`setValidation`, response.data);
          }
        });
    },
    deleteApartment(context, { id }) {
      context.commit(`setDataPreloading`, true);
      return window.axios
        .get(`${backendUrl}/Api/delete/${id}`, {
          headers: {
            Authorization: `Bearer ${context.state.token}`,
          },
        })
        .then((response) => {
          if (response.status === 200) {
            // context.commit('setPage', context.state.pager.pageCount);
            this.dispatch(`getApartments`);
            router.push(`/apartments`);
          }
        });
    },
  },
});

export default store;
