import { createApp } from "vue";
import App from "./App.vue";
import axios from "axios";
import router from "./router.js";
import store from "./state.js";
import PrimeVue from "primevue/config";
import ConfirmationService from "primevue/confirmationservice";
import Tooltip from "primevue/tooltip";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import "primevue/resources/primevue.min.css";
import "primevue/resources/themes/lara-light-teal/theme.css";

window.axios = axios;
createApp(App)
  .use(store)
  .use(router)
  .use(PrimeVue)
  .use(ConfirmationService)
  .directive("tooltip", Tooltip)
  .mount("#app");
