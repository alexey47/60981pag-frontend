import { createRouter, createWebHistory } from "vue-router";
import HomePage from "./components/HomePage";
import Signin from "./components/Signin";
import ApartmentsList from "./components/ApartmentsList";
import LoginDialog from "./components/LoginDialog";
import HelloWorld from "./components/HelloWorld";

const routes = [
  {
    path: "/",
    component: HomePage,
  },
  {
    path: "/signin",
    component: Signin,
  },
  {
    path: "/apartments",
    component: ApartmentsList,
  },
  {
    path: "/login",
    component: LoginDialog,
  },
  {
    path: "/helloworld",
    component: HelloWorld,
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;
